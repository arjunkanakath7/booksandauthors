const Author = require("../schema/authorschema");

const getallauthors = async(req, res) => {
  const author = await Author.find({});
    res.json(author)
  }
const getauthorbyid = async(req, res) => {
  try{
    const author = await Author.findById(req.params.authorID).exec();
    res.status(200).json(author)
  }
  catch(error){
    res.status(404).send('Author is not found')
  }
  }
const addnewauthor = async(req, res) => {
    const authordata = req.body
    const author = new Author(authordata)
    await author.save()
    res.status(201).json(author)
  }
const updateauthor = async(req, res) => {
  try{
    const updateauthor = await Author.findByIdAndUpdate(req.params.authorID, req.body, {new: true})
    res.status(200).json(updateauthor)
  }
   catch(error){
    res.status(404).send("Update author not found")
   }
  }
const deleteauthor = async(req, res) => {
  try{
   await Author.findByIdAndDelete(req.params.authorID)
   res.status(200).send("Deleted")
  }
  catch(error){
    res.status(404).send("author not found")
  }
  }

  module.exports = {
    getallauthors,getauthorbyid,addnewauthor,updateauthor,deleteauthor
  }