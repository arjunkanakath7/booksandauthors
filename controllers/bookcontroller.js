const Book = require("../schema/bookschema");

const getallbooks =async(req, res) => {
  const books = await Book.find({});
    res.json(books)
  }
const getbookbyid = async(req, res) => {
  try{
    const books = await Book.findById(req.params.bookID).exec();
    res.status(200).json()
  }
  catch(error){
   res.status(404).send("Book not found")
  }
  }
const addnewbook = async(req, res) => {
    //1. get data from request body
    const bookdata = req.body
    //2. create document 
    const book = new Book(bookdata)
    //3. save document to database
    await book.save()
    res.status(201).json(book)
  }
const updatebook =  async(req, res) => {
  try{
    const updatebooks =await Book.findByIdAndUpdate( req.params.bookID, req.body, {new: true})
    res.status(200).json(updatebooks)
  }
  catch(error){
    res.status(404).send("Update book is not found")
  }    
  }
const deletebook = async(req, res) => {
    try{
    await Book.findByIdAndDelete(req.params.bookID)
    res.status(200).send("Deleted")
  }
    catch(error){
    res.status(404).send("Book not found")
    }
  }

  module.exports = {
    getallbooks,getbookbyid,addnewbook,updatebook,deletebook
  }