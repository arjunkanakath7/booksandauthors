const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
    name: String,
    price: Number,
    image: String,
    description: String
  });


  const Book = mongoose.model('Book', bookSchema);

  module.exports = Book