const express = require('express')
const { getallbooks, getbookbyid, addnewbook, updatebook, deletebook } = require('../controllers/bookcontroller')
const { addnewauthor } = require('../controllers/authorcontroller')
const router = express.Router()

  //1. Get all books
  router.get('/', getallbooks)
  //2. Get book by ID
  router.get('/:bookID', getbookbyid)

  //3. Add a new book
  router.post('/', addnewbook)

  //4. Update book details
  router.patch('/:bookID',updatebook)

  //5. Delete a book
  router.delete('/:bookID', deletebook)

module.exports = router