const express = require('express')
const { getallauthors, getauthorbyid, addnewauthor, updateauthor, deleteauthor } = require('../controllers/authorcontroller')
const router = express.Router()

 //1. Get all authors
 router.get('/', getallauthors)   
  //2. Get book by ID
  router.get('/:authorID', getauthorbyid) 
  //3. Add a new author
  router.post('/', addnewauthor) 
  //4. Update author details
  router.patch('/:authorID', updateauthor) 
  //5. Delete a author
  router.delete('/:authorID', deleteauthor) 


module.exports = router